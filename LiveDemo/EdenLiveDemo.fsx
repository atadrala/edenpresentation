﻿type Node = Input of int ref * Event<unit>
          | Calc of Node * Node * (int -> int -> int) * bool ref * int ref * Event<unit>

let input value = Input (ref value, Event<unit>() )
let calc node1 node2 f = 
    let dirtyEvent = Event<unit>()
    let cachedValue = ref 0
    let dirtyState = ref true
    let getEvent n = match n with | Input (_, e) -> e | Calc (_,_,_,_,_,e) -> e
    (getEvent node1).Publish.Add( fun () -> dirtyState := true; dirtyEvent.Trigger() )
    (getEvent node2).Publish.Add( fun () -> dirtyState := true; dirtyEvent.Trigger() )
    Calc (node1, node2, f, dirtyState, cachedValue, dirtyEvent)

let rec evaluate node =
    match node with
    | Input (v,_) -> async { return !v }
    | Calc (node1, node2, f, dirtyState, cachedValue, event) ->
        async {
            if (!dirtyState) then
                let! [| v1; v2 |] = Async.Parallel( [evaluate node1; evaluate node2])
                cachedValue := f v1 v2
                dirtyState := false
            return !cachedValue
        }

let setValue node newValue =
    match node with 
    | Input (v,event) -> 
        v := newValue
        event.Trigger()
    | Calc _ -> failwith "Cannot set value of calculation node"

let evaluateAsync node = 
    async {
        let! value = evaluate node
        printf "Evaluation finished %d\n" value
    } |> Async.Start

let i1 = input 1
let i2 = input 2
let i3 = input 3
let c1 = calc i1 i2 (fun v1 v2 -> printf "[%d]C1 evaluated\n" System.Threading.Thread.CurrentThread.ManagedThreadId; v1+v2)
let c2 = calc i2 i3 (fun v1 v2 -> printf "[%d]C2 evaluated\n" System.Threading.Thread.CurrentThread.ManagedThreadId; v1+v2)
let c3 = calc c1 c2 (fun v1 v2 -> printf "[%d]C3 evaluated\n" System.Threading.Thread.CurrentThread.ManagedThreadId; v1*v2)

evaluateAsync c3
evaluateAsync c3

setValue i1 5
evaluateAsync c3
