﻿namespace Eden

open System.Threading.Tasks

module Core = 
    type INode<'t> =  
        abstract member Dirty : IEvent<unit>
        abstract member Evaluate : unit -> Async<'t>

    type Konst<'a>(value :'a) = 
        let event = Event<unit>().Publish
        interface INode<'a> with
            member this.Dirty = event
            member this.Evaluate() = async {return value;}
        
    type InputNode<'a>(defaultValue:'a) = 
        let value = ref defaultValue
        let dirty = Event<unit>()

        let setValue a  = 
            value := a
            dirty.Trigger()

        member this.SetValue = setValue

        interface INode<'a> with
            member this.Dirty = dirty.Publish
            member this.Evaluate() = async { return (!value) }

    type CalcNode<'a,'b,'c>(node1: INode<'a>, node2: INode<'b>, f: 'a->'b->'c) =
        let dirty = ref true
        let lastValue: 'c option ref = ref None
        let event = Event<unit>()
        do 
            node1.Dirty.Add(fun () -> dirty := true; event.Trigger())
            node2.Dirty.Add(fun () -> dirty := true; event.Trigger())
        
        let evaluate = 
            async {
              if !dirty then
                let t1: Task<'a> = node1.Evaluate() |> Async.StartAsTask
                let t2: Task<'b> = node2.Evaluate() |> Async.StartAsTask
                let! v1 = Async.AwaitTask t1
                let! v2 = Async.AwaitTask t2
                lastValue := Some <| f v1 v2
                dirty := false
              return ((!lastValue).Value)
            }

        interface INode<'c> with
            member this.Dirty = event.Publish
            member this.Evaluate() = evaluate

[<AutoOpen>]
module EdenApi =
    open Core
    let private dummyKonst = Konst(())

    // return
    let konst a = Konst(a) :> INode<_>

    // map
    let map node f = CalcNode(dummyKonst, node, fun () -> f) :> INode<_>

    // calc
    let map2 node1 node2 f = CalcNode(node1, node2, f) :> INode<_>

    // calc3, calc4, calc5...
    let map3 node1 node2 node3 f = 
        let nodeInternal = map2 node1 node2 f
        CalcNode(nodeInternal, node3, fun f d -> f d) :> INode<_>

    let map4 node1 node2 node3 node4 f = 
        let nodeInternal = map3 node1 node2 node3 f
        CalcNode(nodeInternal, node4, fun f d -> f d) :> INode<_>

    //project
    //let bind : INode<'a> -> ('a -> INode<'b>) -> INode<'b>
    // flatten
    //let join: INode<INode<'a>> -> INode<'a>

    //let if = map3(ifNode, thenNode, elseNode, fun i t e -> if(i) then t else e)
    //let if2 = 
       // let returnNode = map(ifNode, fun i -> if i then thenNode else elseNode)
        //join(returnNode)
   // let if3 = bind(ifNode, fun i ->  if i then thenNode else elseNode)

    let input a = InputNode a

    let evaluateAsync (node:INode<'a>) = 
        async {
            let! value = node.Evaluate()
            printf "Node value: %A\n" value
        } |> Async.Start