﻿namespace EdenClient

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Markup
 
module Default = 
    [<STAThread>]
    [<EntryPoint>]
    let main argv = 
        let mainWindow = Application.LoadComponent(
                            new System.Uri("/EdenClient;component/MainWindow.xaml", UriKind.Relative)) :?> Window
        mainWindow.DataContext <- new ViewModels.MainWindowViewModel()
        let application = new Application()
        application.Run(mainWindow)