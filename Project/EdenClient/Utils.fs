﻿
[<AutoOpen>]
module Utils
open System.ComponentModel
open Eden.Core

let getThreadId() = (System.Threading.Thread.CurrentThread.ManagedThreadId)

type Output<'a>(node: INode<'a>) as this = 
    let propertyChanged = Event<PropertyChangedEventHandler, PropertyChangedEventArgs>()
    do
      node.Dirty.Add( 
            fun () ->  
                async {
                    this.IsDirty <- true   
                    propertyChanged.Trigger(this, PropertyChangedEventArgs("IsDirty"))
                    let! value = node.Evaluate()
                    this.Value <- Some value
                    propertyChanged.Trigger(this, PropertyChangedEventArgs("Value"))
                    this.IsDirty <- false
                    propertyChanged.Trigger(this, PropertyChangedEventArgs("IsDirty"))
               
                } |> Async.Start)
                             
    member val Value : 'a option = None with get, set
    member val IsDirty : bool = true with get, set

    interface System.ComponentModel.INotifyPropertyChanged with
        member this.add_PropertyChanged(e) =
            propertyChanged.Publish.AddHandler(e)
        member this.remove_PropertyChanged(e) =
            propertyChanged.Publish.RemoveHandler(e)
