﻿module ViewModels
open System.ComponentModel
open Eden
open Utils

type MainWindowViewModel() = 

    let validate tax birthYear = 
        let result = (string tax).StartsWith(string birthYear)       
        printf "[%d] validation %d %d result %A \n" (getThreadId()) tax birthYear result 
        //System.Threading.Thread.Sleep(2000)
        result
        
    let name = input ""
    let taxNumber = input 0
    let email = input ""
    let birthYear = input 0

    let valid = 
        let validation = map2 taxNumber birthYear validate
        Output(validation)

    let summary = 
        let formatter = 
            printf "[%d] Formatting summary message \n" (getThreadId())
            sprintf "Welcome %s!\n TaxNumber: %d\n Email: %s\n age: %d" 
        let age = map birthYear (fun by -> printf "[%d] Calculating age from by %d \n" (getThreadId()) by; System.DateTime.Today.Year - by)
        let summary = map4 name taxNumber email age formatter
        Output(summary)

    member this.Name 
        with set value = name.SetValue value 
    member this.TaxNumber  
        with set value = taxNumber.SetValue value
    member this.Email 
        with set value = email.SetValue value
    member this.BirthYear
        with set value = birthYear.SetValue value

    member this.Summary = summary
    member this.Valid = valid

